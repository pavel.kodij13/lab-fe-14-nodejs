const express = require('express');
const morgan = require('morgan');
const server = express();
const path = require('path');
const fs = require('fs');

server.use(express.json());
server.use(morgan('combined'));

const filesFolder = './postedFiles';

server.post('/api/files', function(req, res){
    try {
        if (!req.body.hasOwnProperty('filename') || req.body.filename.trim() === '') {
            badRequest(res, "Please specify 'filename' parameter");
            return;
        }
        if (!req.body.hasOwnProperty('content')) {
            badRequest(res, "Please specify 'content' parameter");
            return;
        }
        const fileName = req.body.filename;
        if (!checkData(getData(fileName))) {
            badRequest(res, "Unsupported file extension");
            return;
        }
        fs.readdir(filesFolder, (err, files) => {
            if (err) {
               internalServerError(err, res);
                return;
            }
            if (files.includes(fileName)) {
               badRequest(res, "File with '" + fileName + "' filename already exists");
                return;
            }
            createFile(fileName, req.body.content, res);
        });
    } catch (e) {
      internalServerError(e, res);
    }
});

server.get('/api/files', function(req, res){
    try {
        getAllFiles(res);
    } catch (e) {
      internalServerError(e, res);
    }
});

server.get('/api/files/:filename', function(req, res){
    try {
        fs.readdir(filesFolder, (err, files) => {
            if (err) {
               internalServerError(err, res);
                return;
            }
            const fileName = req.params.filename;
            if (!files.includes(fileName)) {
               badRequest(res, "No file with '" + fileName + "' filename found");
                return;
            }
            getFileByName(fileName, res);
        });
    } catch (e) {
         internalServerError(e, res);
    }
});

server.delete('/api/files/:filename', function(req, res){
    try {
        fs.readdir(filesFolder, (err, files) => {
            if (err) {
               internalServerError(err, res);
                return;
            }
            const fileName = req.params.filename;
            if (!files.includes(fileName)) {
               badRequest(res, "No file with '" + fileName + "' filename found");
                return;
            }
            deleteFile(fileName, res);
        });
    } catch (e) {
         internalServerError(e, res);
    }
});

server.put('/api/files/:filename', function(req, res){
    try {
        fs.readdir(filesFolder, (err, files) => {
            if (err) {
               internalServerError(err, res);
                return;
            }
            const fileName = req.params.filename;
            if (!files.includes(fileName)) {
               badRequest(res, "No file with '" + fileName + "' filename found");
                return;
            }
            if (!req.body.hasOwnProperty('filename') || req.body.filename.trim() === '') {
               badRequest(res, "Please specify 'filename' parameter");
                return;
            }
            if (!req.body.hasOwnProperty('content')) {
               badRequest(res, "Please specify 'content' parameter");
                return;
            }
            const newFileName = req.body.filename;
            if (getData(fileName) !== getData(newFileName)) {
               badRequest(res, "File extensions of two filenames are not equal");
                return;
            }
            if (fileName !== newFileName && files.includes(newFileName)) {
               badRequest(res, "File with '" + newFileName + "' new filename already exists");
                return;
            }
            putFile(fileName, newFileName, req.body.content, res);
        });
    } catch (e) {
         internalServerError(e, res);
    }
});

server.listen(8080, function () {
    console.log('Server is running on port 8080')
});


function internalServerError(err, res) {
   res.status(500).send({
       "message": "Server error"
   });
   console.log('500 error: ' + err);
}

function badRequest(res, errMessage) {
   res.status(400).send({
       "message": errMessage
   });
   console.log('400 error: ' + errMessage);
}

function getData(fileName) {
   return (path.extname(fileName)).slice(1);
}

function checkData(extension) {
   return extension === 'txt' || extension === 'log' || extension === 'json' ||
       extension === 'yaml' || extension === 'xml' || extension === 'js';
}

function createFile(fileName, content, res) {
   fs.writeFile(filesFolder + '/' + fileName, content, err => {
       if (err) {
           internalServerError(err, res);
           return;
       }
       res.status(200).send({
           "message": "File created successfully"
       });
   })
}

function getAllFiles(res) {
   fs.readdir(filesFolder, (err, files) => {
       if (err) {
           internalServerError(err, res);
           return;
       }
       if (files.length === 0) {
           badRequest(res, "There are not any files");
           return;
       }
       res.status(200).send({
           "message": "Success",
           "files": files
       });
   });
}

function getFileByName(fileName, res) {
   fs.readFile(filesFolder + '/' + fileName, (err, data) => {
       if (err) {
           internalServerError(err, res);
           return;
       }
       fs.stat(filesFolder + '/' + fileName, (err, stats) => {
           if (err) {
              internalServerError(err, res);
               return;
           }
           res.status(200).send({
               "message": "Success",
               "filename": fileName,
               "content": data.toString(),
               "extension": getData(fileName),
               "uploadedDate": stats.birthtime
           });
       })
   });
}

function deleteFile(fileName, res) {
   fs.unlink(filesFolder + '/' + fileName, (err) => {
       if (err) {
           internalServerError(err, res);
           return;
       }
       res.status(200).send({
           "message": "File deleted successfully"
       });
   });
}

function helpPutFile(newFileName, content, res) {
   fs.writeFile(filesFolder + '/' + newFileName, content, err => {
       if (err) {
           internalServerError(err, res);
           return;
       }
       res.status(200).send({
           "message": "File updated successfully"
       });
   })
}

function putFile(fileName, newFileName, content, res) {
   if (fileName !== newFileName) {
       fs.rename(filesFolder + '/' + fileName, filesFolder + '/' + newFileName, err => {
           if (err) {
              internalServerError(err, res);
               return;
           }
           helpPutFile(newFileName, content, res);
       })
   }
   else helpPutFile(newFileName, content, res);
}
